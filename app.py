from flask import Flask, jsonify, render_template, request
from pymysql import connect, cursors

app = Flask(__name__)
app.config.from_object('config.Base')
connection = connect(cursorclass=cursors.DictCursor, **app.config['DATABASE'])


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
